import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	isSignedIn: false,
	email: "",
};

export const authSlice = createSlice({
	name: "auth",
	initialState,
	reducers: {
		signIn: (state, action) => {
			state.isSignedIn = true;
			state.email = action.payload;
		},
		logOut: (state) => {
			state.isSignedIn = false;
			state.email = "";
		},
	},
});

export const { signIn, logOut } = authSlice.actions;
