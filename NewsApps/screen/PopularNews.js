import React, { useEffect, useState}from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import Card from "../components/Card";
import Header from "../components/Header";
import axios from "axios";


export default function PopularNews() {
    const [ cardPopularData, setCardPopularData ] = useState([]);
    const [ headlinePopularData, setHeadlinePopularData ] = useState([]);
    const fetchPopularData = () => {
        axios.get('https://newsapi.org/v2/everything?q=News&apiKey=fbc6f361447a439ca68c49cc18531822', 
        {
            params: {
                pageSize: 10,
                sortBy: 'relevancy'
            }
        })
        .then((response) => {
            // handle success
            setCardPopularData(response.data.articles);
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
        .finally(function () {
            // always executed
        });
     }

    useEffect(()=> {
        fetchPopularData();
    }, []);

    return (
        <View style={{flex: 1}}>
            <Header />
            <View style={styles.container}>
                <FlatList 
                style={styles.news}
                data={cardPopularData}
                renderItem={({item}) => 
                    <Card 
                        urlToImage={item.urlToImage}
                        title={item.title}
                        author={item.source.name}
                        url={item.url}
                    />}
                    keyExtractor={(item) => item.title}
                />
            </View>
            
        </View>
    ); 
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
      },
    news: {
        flex: 1, 
        width: '100%'
    },
    headline: {
        flex: 1, 
        width: '100%',
        height: 280
    }
});