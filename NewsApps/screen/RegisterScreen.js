import React, { useState } from "react";
import { View, Text, TextInput, StyleSheet, TouchableOpacity, Image, Alert } from "react-native";
import { createUserWithEmailAndPassword } from 'firebase/auth';
import { auth } from '../config/firebase';
import { useDispatch } from "react-redux";
import { signIn } from "../reducer/AuthSlice";

export default function RegisterScreen({navigation}) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [ulangiPassword, setUlangiPassword] = useState("");
    const dispatch = useDispatch();

    const validate = ()=>{
        let newErrors;
        if (!email) {
            newErrors = "Email wajib diisi";
          } else if (ulangiPassword !== password) {
            newErrors = "Password tidak sama";
        }
        return newErrors;
    }
    const submit = () => {
        const findError = validate();

        if(findError !== undefined) { 
            console.log(findError)            
        } else {
            createUserWithEmailAndPassword(auth, email, password)
            .then(()=> {
                Alert.alert("Sign Up Berhasil")
                dispatch(signIn(email))
                
            })
            .catch((error)=>{
                Alert.alert("Register Gagal")
            })
        }
    }
    
    return (
        <View style={styles.container}>
            <View style={styles.textContainer}>
                <Image style={styles.imageLogo} source={require("../assets/icon.png")} />
                <Text> Register Account</Text>
                <TextInput style={styles.inputText} onChangeText={setEmail} value={email} placeholder="Input Email"/>
                <TextInput style={styles.inputText} onChangeText={setPassword} value={password} placeholder="Input Password" secureTextEntry/>
                <TextInput style={styles.inputText} onChangeText={setUlangiPassword} value={ulangiPassword} placeholder="Ulangi Password" secureTextEntry/>
            </View>
            <View>
                <TouchableOpacity style={styles.button} onPress={submit} >
                    <Text style={styles.titleButton}>Sign Up</Text>
                </TouchableOpacity>
                <TouchableOpacity title="Sudah memiliki akun ?" onPress={()=> navigation.navigate("Login")} style={{ justifyContent: 'center', alignItems: 'center',marginTop: 10,}}>
                    <Text>Belum memiliki akun ?</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
      },
      imageLogo: {
        height: 225,
        width: 225,
        marginBottom: 20,
    },
    inputText: {
        borderColor: '#eaeaea',
        borderRadius: 4,
        borderWidth: 1,
        color: '#7d7d7d',
        flexDirection: 'row',
        fontSize: 18,
        height: 54,
        justifyContent: 'center',
        width: 300,
        marginVertical: 8,
        paddingHorizontal: 10,
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 50,
        height: 50,
        width: 300,
        backgroundColor: 'blue',
        borderRadius: 5,
        padding: 10,
    },
    titleButton:{
        color: 'white',
    },
    textContainer: {
        alignItems: 'center',
        padding: 10,
    },
});