import React, { useState, } from "react";
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Image, Alert } from "react-native";
import { auth } from '../config/firebase';
import { useDispatch } from "react-redux";
import { signIn } from "../reducer/AuthSlice";
import { signInWithEmailAndPassword } from "firebase/auth";

export default function LoginScreen({navigation}) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const dispatch = useDispatch();
    const LogIn = ()=> {
        signInWithEmailAndPassword(auth, email, password)
        .then(()=>{
            dispatch(signIn(email))
        })
        .catch((err)=>{
            Alert.alert("Email/Password salah")
            console.log(err)
        })
    }


    return (
        <View style={styles.container}>
            <View style={styles.textContainer}>
                <Image style={styles.imageLogo} source={require("../assets/icon.png")} />
                <Text>Login</Text>
                <TextInput style={styles.inputText} value={email} onChangeText={setEmail}  placeholder="Input Email"/>
                <TextInput style={styles.inputText} value={password} onChangeText={setPassword} placeholder="Input Password"  secureTextEntry />
            </View>
            <View>
                <TouchableOpacity style={styles.button} onPress={LogIn} >
                    <Text style={styles.titleButton}>Sign In</Text>
                </TouchableOpacity>
                <TouchableOpacity title="Belum punya akun ?" onPress={()=> navigation.navigate("Register")} style={{ justifyContent: 'center', alignItems: 'center'}}>
                    <Text>Belum memiliki akun ?</Text>
                </TouchableOpacity>
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
      },
      imageLogo: {
        height: 225,
        width: 225,
        marginBottom: 20,
    },
    inputText: {
        borderColor: '#eaeaea',
        borderRadius: 4,
        borderWidth: 1,
        color: '#7d7d7d',
        flexDirection: 'row',
        fontSize: 18,
        height: 54,
        justifyContent: 'center',
        width: 300,
        marginVertical: 8,
        paddingHorizontal: 10,
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 50,
        marginBottom: 10,
        height: 50,
        width: 300,
        backgroundColor: 'blue',
        borderRadius: 5,
        padding: 10,
    },
    titleButton:{
        color: 'white',
    },
    textContainer: {
        alignItems: 'center',
        padding: 10,
    },
});
