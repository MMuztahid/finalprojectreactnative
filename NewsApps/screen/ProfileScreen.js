import React from 'react';
import { 
    StyleSheet,
    Text,
    View,
    SafeAreaView,
    ScrollView,
    Button
} from 'react-native';
//import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { FontAwesome } from '@expo/vector-icons/';
import { signOut } from 'firebase/auth';
import { useDispatch } from 'react-redux';
import { auth } from '../config/firebase';
import { logOut } from '../reducer/AuthSlice';

export default function ProfileScreen() {
    const dispatch = useDispatch();
    const LogOut = () => {
        signOut(auth)
        .then(()=>dispatch(logOut()))
        .catch((error) => {
            console.log(error)
          });
    }
    return(
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.ScrollView}>
                <View style={styles.container}>
                    <Text style={styles.title}>Tentang Saya</Text>
                    <View style={styles.subtitle}>
                        <FontAwesome name="user-circle" size={200} color="black" />
                    </View>
                    <>
                    <Text style={styles.name}>Muhammad Mujtahid</Text>
                    <Text style={styles.jobs}>React Native Developer</Text>
                    <Text></Text>
                    </>
                    <View style={styles.portofolio}>
                        <View style={styles.contentPortofolio}>
                            <Text>Portofolio</Text>
                            <View style={styles.contentSkill}>
                                <View style={styles.subContentSkill}>
                                    <FontAwesome name="gitlab" size={24} color="black" />
                                    <Text>Gitlab</Text>
                                </View>
                                <View style={styles.subContentSkill}>
                                    <FontAwesome name="github" size={24} color="black" />
                                    <Text>Github</Text>
                                </View>
                            </View>
                        </View>    
                    </View>
                    <View style={styles.socialMedia}>
                        <View style={styles.contentSocialMedia}>
                            <Text>Hubungi Saya</Text>
                            <View style={styles.contentSocialMediaDown}>
                                <View style={styles.subContentSocialMediaDown}>
                                    <View style={styles.subContentSocialMedia}>
                                        <FontAwesome name="facebook-square" size={24} color="black" />
                                        <Text>facebook</Text>
                                    </View>
                                    <View style={styles.subContentSocialMedia}>
                                        <FontAwesome name="instagram" size={24} color="black" />
                                        <Text>instagram</Text>
                                    </View>
                                    <View style={styles.subContentSocialMedia}>
                                        <FontAwesome name="whatsapp" size={24} color="black" />
                                        <Text>whatsapp</Text>
                                    </View>
                                    <View style={styles.subContentSocialMedia}>
                                        <FontAwesome name="linkedin-square" size={24} color="black" />
                                        <Text>LinkedIn</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    <Button title="Log Out" onPress={LogOut} />
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        backgroundColor:'white',
    },
    scrollView: {
        backgroundColor : 'pink',
        marginHorizontal: 20,
    },
    imageLogo:{
        height: 136,
        width: 136,
        resizeMode: 'stretch',
        marginBottom: 20,
        alignSelf: 'center',
        marginTop: 20,
    },
    title:{
        fontSize : 36,
        fontWeight: '700',
        marginBottom: 20,
    },
    subtitle:{
        backgroundColor: 'grey',
        height: 200,
        width: 200,
        borderRadius: 100,
    },
    name:{
        fontSize: 24,
         fontWeight: '700',
         color:'blue',
    },
    jobs:{
        fontSize: 16,
         fontWeight: '400',
         color:'blue',
    },
    portofolio:{
        backgroundColor: '#EFEFEF',
        height: 140,
        width: 300,
    },
    contentPortofolio:{
        padding: 5,
        //borderBottomWidth: 1,
        //borderBottomColor: 'grey',
    },
    contentSkill:{
        padding: 10,
        //backgroundColor: 'yellow',
        flexDirection: 'row',
        justifyContent: 'space-around',
        height: 90,
        marginTop: 10,
    },
    subContentSkill: {
        flexDirection: 'column',
        paddingBottom: 5,
    },
    socialMedia: {
        marginTop: 10,
        backgroundColor: '#EFEFEF',
        height: 300,
        width: 300,
    },
    contentSocialMedia:{
        padding: 8,
    },
    contentSocialMediaDown: {
        flexDirection: 'column',
        justifyContent: 'space-around',
    },
    subContentSocialMediaDown: {
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: 10,
    },
    subContentSocialMedia: {
        flexDirection: 'column',
        padding: 10,
    }
})