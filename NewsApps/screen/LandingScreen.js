
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { FontAwesome } from '@expo/vector-icons';
import Bookmark from "./Bookmark";
import NewsHome from "./NewsHome";
import PopularNews from "./PopularNews";
import SearchNews from "./SearchNews";



export default function HomeScreen({navigation}) {
    const Tab =  createBottomTabNavigator();
    return (
        <Tab.Navigator 
        screenOptions={({ route }) => ({
            tabBarIcon: ({ color, size }) => {
              let iconName;
  
              if (route.name === 'Home') {
                iconName = 'home';
              } else if (route.name === 'Popular') {
                iconName = 'paper-plane';
                size = 20;
              } else if (route.name === 'Search') {
                iconName = 'search';
                size = 20;
              } else if (route.name === 'Bookmark') {
                iconName = 'star';
                size = 20;
              }
  
              // You can return any component that you like here!
              return <FontAwesome name={iconName} size={size} color={color} />;
            },
            tabBarActiveTintColor: '#22A0E7',
            tabBarInactiveTintColor: 'black',
          })}>
            <Tab.Screen name="Home" component={NewsHome} options={{headerShown: false }}/>
            <Tab.Screen name="Popular" component={PopularNews} options={{headerShown: false }}/>
            <Tab.Screen name="Search" component={SearchNews} options={{headerShown: false }}/>
            <Tab.Screen name="Bookmark" component={Bookmark} options={{headerShown: false }}/>
        </Tab.Navigator>
        
    )
}

