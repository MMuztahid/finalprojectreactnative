import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";
import Header from "../components/Header";


export default function Bookmark() {
    return (
        <View style={{flex: 1}}>
            <Header />
            <View style={styles.container}>
                <Text>Bookmark</Text>
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
      },
});