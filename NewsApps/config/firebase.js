import { getApps, initializeApp } from 'firebase/app';
//import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { initializeAuth, getReactNativePersistence} from 'firebase/auth/react-native';

export const firebaseConfig = {
    apiKey: "AIzaSyAtPALnOAUZnjaql7cMHfLbCM3rHo7clsE",
    authDomain: "newsapps-dbdd3.firebaseapp.com",
    projectId: "newsapps-dbdd3",
    storageBucket: "newsapps-dbdd3.appspot.com",
    messagingSenderId: "915371013468",
    appId: "1:915371013468:web:be3a176d2987ac2856fd99"
  };

  let app;
  if (!getApps().length) {
    app = initializeApp(firebaseConfig);
  } 
  //export const auth = getAuth(app);
export const auth = initializeAuth(app, {
  persistence: getReactNativePersistence(AsyncStorage)
  });
  export const db = getFirestore(app);