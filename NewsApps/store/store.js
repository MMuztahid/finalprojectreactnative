import { configureStore } from "@reduxjs/toolkit";
import { authSlice } from "../reducer/AuthSlice";

export const store = configureStore({
	reducer: {
        [authSlice.name] : authSlice.reducer,		
	},
});