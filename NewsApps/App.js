import { Provider } from 'react-redux';
import { store } from './store/store';
import RootNavigator from './components/RootNavigator';
import { SafeAreaView } from 'react-native-safe-area-context';

export default function App() {
  return (
    <SafeAreaView style={{flex: 1}}>
    <Provider store={store}>
      <RootNavigator />
    </Provider>
    </SafeAreaView>
  );
}
