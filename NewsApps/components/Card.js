import React from "react";
import { View, Text, Image, StyleSheet, Pressable } from "react-native";
import * as WebBrowser from 'expo-web-browser';

export default function Card(props) {
    const goToSource = () => {
        WebBrowser.openBrowserAsync(props.url);
    }
    return (
        <Pressable style={styles.container} onPress={goToSource}>
            <Image source={{
                uri: props.urlToImage
            }} 
            style={styles.image}/>
            <View style={{width: '50%', height: 120, justifyContent: 'center', paddingLeft: 20}}>
                <Text numberOfLines={2}>{props.title}</Text>
                <Text>{props.author}</Text>
            </View>
            
        </Pressable>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: 130,
        width: '100%',
        padding: 5,
        backgroundColor: '#f5f5f5',
        justifyContent: 'center',
      },
      image: {
        height: 120,
        width: "40%",
      }
});

