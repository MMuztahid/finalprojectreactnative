import React from "react";
import { View, Text, ImageBackground, StyleSheet } from "react-native";

export default function Headline(props) {
    return (
        <View style={styles.container}>
            <ImageBackground source={{
                uri: props.urlToImage
            }} 
            style={styles.image}>
                <View style={{ width: '100%', paddingLeft: 20, backgroundColor: '#00000050', paddingBottom: 20 }}>
                    <Text style={styles.title} numberOfLines={1}>{props.title}</Text>
                    <Text style={styles.desc} numberOfLines={3}>{props.author}</Text>
                </View>                
            </ImageBackground>
            <Text style={styles.titleNews}>{props.textTitle}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#eee',
        alignItems: 'center',
        width: '100%',
        height: 280
      },
      image: {
        height: 200,
        width: "100%",
        alignItems: 'flex-start',
        justifyContent: 'flex-end'
      },
      title: {
        color: 'white',
        fontSize: 24,
        lineHeight: 36,
        fontWeight: 'bold',
      },
      titleNews: {
        color: 'black',
        fontSize: 42,
        lineHeight: 60,
        fontWeight: 'bold',
      },
      desc: {
        color: 'white',
        fontWeight: 'bold',
      }
});

