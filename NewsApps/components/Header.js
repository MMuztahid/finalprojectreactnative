import React from "react";
import { View, TextInput, TouchableOpacity } from "react-native";
import { FontAwesome } from "@expo/vector-icons"
import { useNavigation } from "@react-navigation/native";

export default function Header() {
    const navigation = useNavigation();
    return (
        <View style={{
            height: 48,
            backgroundColor: '#22A0E7',
            justifyContent: 'center',
            paddingLeft: 40,
            paddingRight: 10,
        }}>
            <View style={{flexDirection: 'row' , alignItems:'center', justifyContent: 'space-between'}}>
                <View style={{flexDirection: 'row' , alignItems:'center'}}>
                    <TextInput 
                    placeholder="Find News" 
                    style={{ 
                        width: 160, 
                        height: 36,backgroundColor:'white', 
                        alignItems: 'flex-end', 
                        paddingLeft: 10, 
                        marginRight: 10 }} />
                    <TouchableOpacity>
                        <FontAwesome name="search" size={24} />
                        </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={()=>navigation.navigate("Profile")}>
                    <FontAwesome name="user-circle" size={32} />
                </TouchableOpacity>
            </View>
        </View>
    );
}