import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import { useSelector } from "react-redux";
import HomeScreen from "../screen/LandingScreen";
import LoginScreen from "../screen/LoginScreen";
import ProfileScreen from "../screen/ProfileScreen";
import RegisterScreen from "../screen/RegisterScreen";

const Stack = createNativeStackNavigator();

export default function RootNavigator() {
    const isSignedIn = useSelector((state) => state.auth.isSignedIn);
    return (
        <NavigationContainer>
            <Stack.Navigator>
                {isSignedIn ? (
                    <>
                        <Stack.Screen name="Landing" component={HomeScreen} options={{headerShown: false }}/>
                        <Stack.Screen name="Profile" component={ProfileScreen} />
                    </>
                ) : (
                    <>
                        <Stack.Screen name="Login" options={{ headerShown: false }} component={LoginScreen}  />
                        <Stack.Screen name="Register" options={{ headerShown: false }} component={RegisterScreen} />
                    </>
                )}
            </Stack.Navigator>
        </NavigationContainer>
    );
}
